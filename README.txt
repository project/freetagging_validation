This is a simple module for validating user input
for Taxonomy freetagging field widget.

The concept is to validate on user input string
text before it is passed to taxonomy validator
and turned into a taxonomy terms.

The field validation module validation comes too
late since all the user input data has already
been converted to taxonomy tid id.

How to install
==============

Install as normal Drupal module would.
There are no configuration needed as all of the
validation rules is using field validation rules
thus you can utilize field validation GUI to conf
igure the rules you want to apply to any taxonomy
freetagging widget.